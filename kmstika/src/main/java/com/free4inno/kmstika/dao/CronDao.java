package com.free4inno.kmstika.dao;

import com.free4inno.kmstika.domain.Cron;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Author HUYUZHU.
 * Date 2021/3/27 21:45.
 */

@Repository
public interface CronDao extends JpaRepository<Cron, Integer> {

    Cron findByCronId(Integer cronId);

}

package com.free4inno.knowledgems.service;

import com.alibaba.fastjson.JSON;
import com.free4inno.knowledgems.dao.LabelInfoDao;
import com.free4inno.knowledgems.domain.LabelInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author HUYUZHU.
 * Date 2021/3/27 22:05.
 */

@Slf4j
@Service
public class LabelService {

    @Autowired
    LabelInfoDao labelInfoDao;

    // labelIds从一维List转为按照一个一级标签一条的二维List
    public ArrayList<ArrayList<Map<String, Object>>> switchLabelInfos(ArrayList<String> labelIds) {
        log.info(this.getClass().getName() + "----in----" + "将参数labelIds从一维List转为按照一个一级标签一条的二维List" + "----");
        ArrayList<ArrayList<Map<String, Object>>> searchLabelInfos = new ArrayList<>();
        List<LabelInfo> labelInfoList1 = labelInfoDao.findByUplevelId(0); //遍历一个包含全部一级标签的List
        labelInfoList1.forEach(item -> {
            ArrayList<Map<String, Object>> labelInfoList2 = new ArrayList<>();
            labelIds.forEach(labelId -> {
                Map<String, Object> labelInfoMap = new HashMap<>();
                LabelInfo labelInfo = labelInfoDao.findAllById(Integer.parseInt(labelId));
                if (labelInfo.getUplevelId() == item.getId()) {
                    labelInfoMap = JSON.parseObject(JSON.toJSONString(labelInfo), Map.class);
                    labelInfoList2.add(labelInfoMap);
                }
            });
            if (labelInfoList2.size() != 0) {//如果该一级标签下有搜索条件则添加，无搜索条件则不添加
                labelInfoList2.add(0, JSON.parseObject(JSON.toJSONString(item), Map.class));
                searchLabelInfos.add(labelInfoList2);
            }
        });
        log.info(this.getClass().getName() + "----out----" + "返回完成转换后的二维List" + "----");
        return searchLabelInfos;
    }

    // 全部的标签：按照一个一级标签一条的二维List
    public List<List<Map<String, String>>> getLabels() {
        log.info(this.getClass().getName() + "----in----" + "将全部labelIds从一维List转为按照一个一级标签一条的二维List" + "----");
        List<LabelInfo> labelInfos = labelInfoDao.findAll();
        List<List<Map<String, String>>> tagList = new ArrayList<>();
        for (LabelInfo labelInfo1 : labelInfos) {
            if (labelInfo1.getUplevelId() == 0) {
                List<Map<String, String>> tagLine = new ArrayList<>();
                Map<String, String> infoMap1 = new HashMap<>();
                infoMap1.put("uplevelid", String.valueOf(labelInfo1.getUplevelId()));
                infoMap1.put("id", String.valueOf(labelInfo1.getId()));
                infoMap1.put("name", labelInfo1.getLabelName());
                tagLine.add(infoMap1);
                int headId = labelInfo1.getId();
                for (LabelInfo labelInfo2 : labelInfos) {
                    if (labelInfo2.getUplevelId() == headId) {
                        Map<String, String> infoMap2 = new HashMap<>();
                        infoMap2.put("uplevelid", String.valueOf(labelInfo2.getUplevelId()));
                        infoMap2.put("id", String.valueOf(labelInfo2.getId()));
                        infoMap2.put("name", labelInfo2.getLabelName());
                        tagLine.add(infoMap2);
                    }
                }
                tagList.add(tagLine);
            }
        }
        log.info(this.getClass().getName() + "----out----" + "返回完成转换后的二维List" + "----");
        return tagList;
    }
}

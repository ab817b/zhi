package com.free4inno.knowledgems.dao;

import com.free4inno.knowledgems.domain.GroupInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Author HaoYi.
 * Date 2020/9/11.
 */
@Repository
public interface GroupInfoDao extends JpaRepository<GroupInfo, Integer> {
    Optional<GroupInfo> findById(Integer id);

    Page<GroupInfo> findAll(Pageable pageable);

    GroupInfo findAllById(Integer id);

    GroupInfo findTopByOrderByIdDesc();

    Optional<GroupInfo> findByGroupName(String groupName);
}

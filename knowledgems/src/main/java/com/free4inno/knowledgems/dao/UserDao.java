package com.free4inno.knowledgems.dao;

import com.free4inno.knowledgems.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Author HaoYi.
 * Date 2020/9/11.
 */
@Repository
public interface UserDao extends JpaRepository<User, Integer> {
    Optional<User> findById(Integer id);

    Optional<User> findByAccount(String account);

    Optional<User> findByRealName(String realName);

    User findByAccountAndUserPassword(String account, String psw);

    User findByMailAndUserPassword(String mail, String psw);

    Optional<User> findAllByMail(String mail);

    Optional<User> findAllByAccount(String tel);

    Optional<User> findAllByAppKey(String appKey);

    Page<User> findAllByRoleId(Integer roleId, Pageable pageable);

    Page<User> findAllByRoleIdIsNot(Integer roleId, Pageable pageable);

    Page<User> findAllByRoleIdAndAccountLikeOrRoleIdAndRealNameLikeOrRoleIdAndMailLike(
            Integer roleId1, String account, Integer roleId2, String realName, Integer roleId3, String mail, Pageable pageable);

    Page<User> findAllByRoleIdIsNotAndAccountLikeOrRoleIdIsNotAndRealNameLikeOrRoleIdIsNotAndMailLike(
            Integer roleId1, String account, Integer roleId2, String realName, Integer roleId3, String mail, Pageable pageable);

    int countAllByRoleIdIsNot(Integer roleId);

    int countAllByRoleId(Integer roleId);
}

package com.free4inno.kmses;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KmsesApplication {

    public static void main(String[] args) {
        SpringApplication.run(KmsesApplication.class, args);
    }

}
